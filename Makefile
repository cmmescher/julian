.POSIX:

ifndef PREFIX
  PREFIX = /usr/local
endif
MANPREFIX = $(PREFIX)/share/man

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin; \
	cp -f julian $(DESTDIR)$(PREFIX)/bin/; \
	chmod 755 $(DESTDIR)$(PREFIX)/bin/julian

	mkdir -p $(DESTDIR)$(MANPREFIX)/man1; \
	cp -f julian.1.gz $(DESTDIR)$(MANPREFIX)/man1/julian.1.gz; \
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/julian.1.gz

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/julian

	rm -f $(DESTDIR)$(MANPREFIX)/man1/julian.1.gz

.PHONY: install uninstall
