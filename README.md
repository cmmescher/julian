# Julian
Julian is a simple POSIX compliant shell script to convert dates between
calendars.

## Installation

``` sh
git clone https://gitlab.com/cmmescher/julian
cd julian
sudo make install
```

For users of **Arch**-based distributions, Julian is available on the AUR as [julian-git](https://aur.archlinux.org/packages/julian-git).

## Dependencies

- `sh` -- run the script (any POSIX compliant `sh` will work. I recommend `dash`).

## Usage

`julian` converts dates from one calendar to another, using the Julian, Revised
Julian, and Gregorian calendars. By default it converts from the Gregorian
calendar to the Julian calendar. It requires the year, month, and day to be
specified either with stdin, with the flag `-d`, or as arguments.

The calendar of the input is specified using one of the `-j`, `-r`, or `-g`
flags, while the calendar for the output is specified using one of the `-J`,
`-R`, or `-G` flags. If multiple from either group are given (they shouldn't
be), then the Julian calendar has precedence, followed by the Revised Julian,
and last the Gregorian. Using the flags `-gJ` has the default behavor as though
no flags were specified, convering from the Gregorian to the Julian calendar.

`julian` can also print out the weekday when either the `-w` or `-W` flags are
specified. With `-w` the weekday is output as a number between 1 and 7 with 1
indicating Sunday. If `-W` is specified it requires a format specifier to be
given. This can take the form of how it would output the day Monday, for example
`M`, `Mon`, or `Monday` for one letter, three letter, or full day name output
respectively.

`julian` can also use the Byzantine Calendar's Anno Mundi style year if given
the `-b` or `-B` flags. `-b` indicates that the input date was given in the A.M.
date style year, while `-B` indicates that the date should be output using the
A.M. style year

`julian` can take the date input in several ways. It can receive the date in the
format `<YEAR>/<MONTH>/<DAY>` to stdin, or after the `-d` flag. It can also take
`<YEAR>` `<MONTH>` `<DAY>` as command line arguments.

The `-i` and `-o` flags allow specifying a different separator, provided after
the flag. By default `julian` uses a forward slash (/) as a separator.

`julian` can also add to or subtract days from the output date. If the same
input and output calendars are specified this allows calculating the date a
certain number of days from the input date. The number of days to add or
subtract are given with the `-s` flag.

`julian` can also output whether the input calendar or output calendar has a
leap day in the input/output year if given the `-l` or `-L` flags respectively.

If given the `-v` flag, `julian` will print out the name associated with each
date or calculation.

Please see `man julian` for more information.

## Compatibility

Pascha has only been tested on GNU+Linux systems. It may work on MacOS, BSD,
or other Unix-like operating systems, but it may not. If you do get it to run on
any of these, please let me know, so this can be updated.

## Version


- v1.1

Released December 09th, 7521/12/09 (2022) O.S. (December 22nd, 2022 N.S.).

- v1.0

First released publicly on May 11, 7529 (May 24, 2021 N.S.)

## Author

Written by Christopher Michael Mescher originally in 2021.

## Copyright and License

Copyright © 2021, 2022 Christopher Michael Mescher.

License [GPLv3+](https://gnu.org/licenses/gpl.html)

## Project status

The project is mostly complete. No new features are planned, but bugs can be
reported to `echo \<pascha-mescher+faith\>|sed s/\+/./g\;s/\-/@/` and they will
be worked on.

## Changelog

- v1.1
  - Fix bug in calculation of JDN for Revised Julian dates.
